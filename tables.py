import unittest
from selenium import webdriver
from selenium.webdriver.chrome.service import Service as ChromeService
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager
from prettytable import PrettyTable


class TablesTests(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome(
            service=ChromeService(ChromeDriverManager().install()))
        driver = self.driver
        driver.get('http://the-internet.herokuapp.com/')
        driver.maximize_window()
        driver.implicitly_wait(2)
        driver.find_element(By.LINK_TEXT, 'Sortable Data Tables').click()

    def test_sort_tables(self):
        driver = self.driver

        rows = []
        table = PrettyTable()

        for i in range(5):
            header = driver.find_element(
                By.XPATH, f'//*[@id="table1"]/thead/tr/th[{i + 1}]/span')

            for j in range(4):
                row_data = driver.find_element(
                    By.XPATH, f'//*[@id="table1"]/tbody/tr[{j + 1}]/td[{i + 1}]')
                rows.append(row_data.text)

            table.add_column(header.text, rows)
            rows.clear()

        print()
        print(table)

    def tearDown(self):
        self.driver.quit()


if __name__ == '__main__':
    unittest.main(verbosity=2)
