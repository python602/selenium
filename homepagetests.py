import unittest
from selenium import webdriver
from selenium.webdriver.chrome.service import Service as ChromeService
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager


class HomePageTests(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome(
            service=ChromeService(ChromeDriverManager().install()))
        driver = self.driver
        driver.get('http://demo-store.seleniumacademy.com/')
        driver.maximize_window()
        driver.implicitly_wait(15)

    def test_search_text_fild(self):
        search_field = self.driver.find_element(value='search')

    def test_search_text_text_field_by_name(self):
        search_field = self.driver.find_element(by=By.NAME, value='q')

    def test_search_text_field_by_class_name(self):
        search_field = self.driver.find_element(
            by=By.CLASS_NAME, value='input-text')

    def tesT_search_button_enabled(self):
        button = self.driver.find_element(by=By.CLASS_NAME, value='button')

    def test_count_of_promo_banner_images(self):
        banner_list = self.driver.find_element(
            by=By.CLASS_NAME, value='promos')
        banners = banner_list.find_elements(by=By.TAG_NAME, value='img')
        self.assertEqual(3, len(banners))

    def test_vip_promo(self):
        vip_promo = self.driver.find_element(
            by=By.XPATH, value='//*[@id="top"]/body/div/div[2]/div[2]/div/div/div[2]/div[1]/ul/li[4]/a/img')

    def test_shopping_cart(self):
        shopping_cart_icon = self.driver.find_element(by=By.CSS_SELECTOR, value='div.header-minicart span.icon')

    def tearDown(self):
        self.driver.quit()


if __name__ == '__main__':
    unittest.main(verbosity=2)
