import unittest
from selenium import webdriver
from selenium.webdriver.chrome.service import Service as ChromeService
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager


class RegisterNewUser(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome(
            service=ChromeService(ChromeDriverManager().install()))
        driver = self.driver
        driver.get('http://demo-store.seleniumacademy.com/')
        driver.maximize_window()
        driver.implicitly_wait(30)

    def test_new_user(self):
        driver = self.driver
        driver.find_element(
            by=By.XPATH, value='//*[@id="header"]/div/div[2]/div/a/span[2]').click()
        driver.find_element(by=By.LINK_TEXT, value='Log In').click()

        create_account_button = driver.find_element(
            by=By.XPATH, value='//*[@id="login-form"]/div/div[1]/div[2]/a/span/span')
        self.assertTrue(create_account_button.is_displayed()
                        and create_account_button.is_enabled())
        create_account_button.click()

        self.assertEqual('Create New Customer Account', driver.title)

        first_name = driver.find_element(by=By.ID, value='firstname')
        middle_name = driver.find_element(by=By.ID, value='middlename')
        last_name = driver.find_element(by=By.ID, value='lastname')
        email_address = driver.find_element(by=By.ID, value='email_address')
        news_letter_subscription = driver.find_element(
            by=By.ID, value='is_subscribed')
        password = driver.find_element(by=By.ID, value='password')
        confirm_password = driver.find_element(by=By.ID, value='confirmation')
        submit_button = driver.find_element(
            by=By.XPATH, value='//*[@id="form-validate"]/div[2]/button/span/span')

        self.assertTrue(first_name.is_enabled()
                        and middle_name.is_enabled()
                        and last_name.is_enabled()
                        and email_address.is_enabled()
                        and news_letter_subscription.is_enabled()
                        and password.is_enabled()
                        and confirm_password.is_enabled()
                        and submit_button.is_enabled())

        first_name.send_keys('Test')
        middle_name.send_keys('Test')
        last_name.send_keys('Test')
        email_address.send_keys('test@testing.com')
        password.send_keys('123456789')
        confirm_password.send_keys('123456789')
        submit_button.click()

    def tearDown(self):
        self.driver.quit()


if __name__ == '__main__':
    unittest.main(verbosity=2)
