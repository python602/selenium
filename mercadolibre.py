import unittest
from selenium import webdriver
from selenium.webdriver.chrome.service import Service as ChromeService
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager
from time import sleep
from prettytable import PrettyTable


class MercadolibreTests(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome(
            service=ChromeService(ChromeDriverManager().install()))
        driver = self.driver
        driver.get('https://www.mercadolibre.com')
        driver.maximize_window()
        driver.implicitly_wait(15)

    def test_search_ps4(self):
        driver = self.driver

        country = driver.find_element(By.ID, 'VE')
        country.click()
        sleep(2)

        search_field = driver.find_element(By.NAME, 'as_word')
        search_field.click()
        search_field.clear()
        search_field.send_keys('playstation 5')
        search_field.submit()
        sleep(2)

        location = driver.find_element(
            By.PARTIAL_LINK_TEXT, 'Distrito Capital')
        location.click()
        sleep(2)

        condition = driver.find_element(By.PARTIAL_LINK_TEXT, 'Nuevo')
        condition.click()
        sleep(2)

        order_by = 'Mayor precio'
        order_menu = driver.find_element(
            By.XPATH, '//*[@id="root-app"]/div/div[2]/section/div[2]/div[2]/div/div/div[2]/div/div/button/span')
        order_menu.click()
        dropdown_options = driver.find_elements(
            By.CLASS_NAME, 'andes-list__item-primary')
        for i in range(len(dropdown_options)):
            if dropdown_options[i].text == order_by:
                dropdown_options[i].click()
        sleep(2)

        table = PrettyTable()
        headers = ['name', 'price']
        articles = []
        prices = []

        for i in range(5):
            article_name = driver.find_element(
                By.XPATH, f'//*[@id="root-app"]/div/div[2]/section/ol/li[{i + 1}]/div/div/div[2]/div[1]/a/h2').text
            articles.append(article_name)
            article_currency = driver.find_element(
                By.XPATH, f'//*[@id="root-app"]/div/div[2]/section/ol/li[{i + 1}]/div/div/div[2]/div[2]/div[1]/div/div/div/div/span[1]/span[2]/span[1]').text
            article_price = driver.find_element(
                By.XPATH, f'//*[@id="root-app"]/div/div[2]/section/ol/li[{i + 1}]/div/div/div[2]/div[2]/div[1]/div/div/div/div/span[1]/span[2]/span[2]').text
            prices.append(f'{article_currency} {article_price}')
        table.add_column(headers[0], articles)
        table.add_column(headers[1], prices)

        print()
        print(table)

    def tearDown(self):
        self.driver.quit()


if __name__ == '__main__':
    unittest.main(verbosity=2)
