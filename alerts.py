import unittest
from selenium import webdriver
from selenium.webdriver.chrome.service import Service as ChromeService
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager


class AlertsTests(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome(
            service=ChromeService(ChromeDriverManager().install()))
        driver = self.driver
        driver.get('http://demo-store.seleniumacademy.com/')
        driver.maximize_window()
        driver.implicitly_wait(15)

    def test_compate_productos_removal_alert(self):
        driver = self.driver
        search_field = driver.find_element(by=By.NAME, value='q')
        search_field.clear()

        search_field.send_keys('tee')
        search_field.submit()

        driver.find_element(by=By.CLASS_NAME, value='link-compare').click()
        driver.find_element(by=By.LINK_TEXT, value='Clear All').click()

        alert = driver.switch_to.alert
        alert_text = alert.text

        self.assertEqual(
            'Are you sure you would like to remove all products from your comparison?', alert_text)

        alert.accept()

    def tearDown(self):
        self.driver.quit()


if __name__ == '__main__':
    unittest.main(verbosity=2)
